package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Graph{
	// Map represented by a set of nodes
	private ArrayList<HashSet<Integer>> nodes;
	// Travel time
	private int time[][];
	@SuppressWarnings("unused")
	private String [] names;
	private boolean coal[];
		
	/**
	 * Build a Map Object full of isolated nodes.
	 * @param numberOfNodes the number of nodes the new Map will contain.
	 */
	public Graph(int numberOfNodes){		
		nodes = new ArrayList<HashSet<Integer>>();
		coal = new boolean[numberOfNodes];
		time = new int[numberOfNodes][numberOfNodes]; 
		
		for(int i=0; i<numberOfNodes; ++i) {
			nodes.add(new HashSet<Integer>());
		}
	}
	
	public void addNames(String [] name) {
		names = name;
	}
	
	public void addCoal(boolean hasCoal, int node) {
		 coal[node] = hasCoal;
	}
	
	/**
	 * Add a tunnel from node i to node j, and also add the travel time.  
	 * @param i represents the starting node.
	 * @param j represents the destination node. 
	 * @param time represents the travel time from node i to node j.
	 */
	public void addTunnel(int i, int j, int travelTime){
		checkNodes(i, j);
		checkTunnelDoesntExists(i, j);
		nodes.get(i).add(j);
		time[i][j] = travelTime;	
	}		
	
	/**
	 * Erase the tunnel that connects node i with node j.
	 * @param i represents the starting node.
	 * @param j represents the destination node.
	 */
	public void eraseTunnel(int i, int j){
		checkNodes(i, j);
		checkTunnelExists(i, j);		
		nodes.get(i).remove(j);	
	}

	/**
	 * Check if a tunnel connecting nodes i and j exists.
	 * @param i represents the starting node.
	 * @param j represents the destination node.
	 * @return true if the tunnel exists, false if it doesn't.
	 */
	public boolean tunnelExists(int i, int j){
		checkNodes(i, j);
		return nodes.get(i).contains(j);
	}
	
	/**
	 * Returns travel time from node i to node j.
	 * @param i represents the starting node.
	 * @param j represents the destination node.
	 * @return (double) time.
	 */
	public double travelTime(int i, int j){
		checkTunnelExists(i, j);
		return time[i][j];
	}	
	
	/**
	 * Given a node, returns all his neighbors.
	 * @param i represents the node.
	 * @return (set) Neighbors of the i node.
	 */
	public Set<Integer> neighbors(int i){
		checkNodeExists(i);
		return nodes.get(i); // :)
	}
	
	/**
	 * Returns the number of nodes of a Map instance.
	 * @return (int) Number of nodes.
	 */
	public int nodesCount(){
		return nodes.size();
	}
	
	public boolean getCoal(int index) {
		return coal[index];
	}

	/**
	 * Returns the number of tunnels of a Map instance.
	 * @return (int) Number of tunnels.
	 */
	public int tunels(){
		int ret = 0;
		int nodes = nodesCount();
		for(int i=0; i<nodes; ++i) {
			for(int j=i+1; j<nodes; ++j) if( tunnelExists(i,j) ) {
				++ret;
			}
		}		
		return ret;
	}
		
	/**
	 * Check if nodes i and j exists, and if they are different.
	 * @param i, first node.
	 * @param j, second node.
	 */
	private void checkNodes(int i, int j){
		int nodes = nodesCount();
		
		if( i < 0 || i >= nodes) {
			throw new IllegalArgumentException("Se intenta acceder a un punto "
					+ "de interes fuera de rango! i = " + i);
		}

		if( j < 0 || j >= nodes) {
			throw new IllegalArgumentException("Se intenta acceder a un punto "
					+ "de interes fuera de rango! i = " + j);
		}
		
		if( i == j ) {
			throw new IllegalArgumentException("Se intenta acceder a dos nodos"
					+ "iguales, i=" + i + ", y j=" + j);
		}
	}

	/**
	 * Given two nodes, checks the existence of a tunnel between them.
	 * If the tunnel exists, throws an Illegal Argument Exception.
	 * @param i represents the starting node.
	 * @param j represents the destination node. 
	 */
	private void checkTunnelDoesntExists(int i, int j){
		if( tunnelExists(i, j) )
			throw new IllegalArgumentException("El tunel (" + i + ", " + j
					+ ") ya existe!");
	}
	
	/** 
	 * Given two nodes, checks the existence of a tunnel between them.
	 * If the tunnel doesn't exist, throws an Illegal Argument Exception.
	 * @param i represents the starting node.
	 * @param j represents the destination node. 
	 */
	protected void checkTunnelExists(int i, int j){
		if( !tunnelExists(i, j) )
			throw new IllegalArgumentException("El tunel (" + i + ", " + j
					+ ") no existe!");
	}

	/**
	 * Given a node, checks if it is part of the Map instance.
	 * @param i represents a node.
	 * If the node doesn't exist, throws an Illegal Argument Exception.
	 */
	private void checkNodeExists(int i){
		if( i < 0 || i >= nodesCount())
			throw new IllegalArgumentException("El punto de interés no "
				+ "existe. i = " + i);
	}
	
	public int[][] getTimes(){
		return time;
	}
}





