package model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("unused")
public class dijkstraTest {
	
private Graph _grafo;
private Graph _grafo2;

	@Before
	public void inicializar()
	{
		_grafo = new Graph(5);
		_grafo.addTunnel(0, 1, 3);
		_grafo.addTunnel(0, 3, 2);
		_grafo.addTunnel(1, 2, 4);
		_grafo.addTunnel(1, 3, 1);
		_grafo.addTunnel(2, 4, 4);
		_grafo.addTunnel(3, 4, 2);		
		_grafo.addTunnel(4, 0, 8);	
		_grafo.addCoal(true, 3);
		
		_grafo2 = new Graph(6);
		
		_grafo2.addTunnel(0, 1, 3);
		_grafo2.addTunnel(0, 3, 2);
		_grafo2.addTunnel(0, 2, 8);
		_grafo2.addTunnel(1, 2, 1);
		_grafo2.addTunnel(2, 3, 3);
		_grafo2.addTunnel(3, 4, 3);
		_grafo2.addTunnel(4, 1, 3);
		_grafo2.addTunnel(4, 0, 1);
		_grafo2.addTunnel(0, 5, 1);
		_grafo2.addCoal(true, 3);
	}

	@Test
	public void test() {
		System.out.println("Graph");
		Dijkstra.solveGraph(_grafo, 0);
		System.out.println("Graph2");
		Dijkstra.solveGraph(_grafo2, 0);
	}

}
