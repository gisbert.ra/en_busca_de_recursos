package model;

import java.util.ArrayList;

public class Dijkstra {

	private static Graph _graph;
	private static int _distanceArray[];
	private static Boolean _visited[];
	private static int _parents[];
	private static ArrayList<Integer> _candidates;
	private static int _startingNode;

	public static void solveGraph(Graph graph, int startingNode) {
		int nodesCount = graph.nodesCount();
		_parents = new int[nodesCount];
		_graph = graph;
		_distanceArray = new int[nodesCount];
		_visited = new Boolean[nodesCount];
		_startingNode = startingNode;

		// Initialize all distances as INFINITE and visited[] as false
		for (int i = 0; i < nodesCount; i++) {
			_distanceArray[i] = Integer.MAX_VALUE;
			_visited[i] = false;
			_parents[i] = -1;
		}

		// Distance of source node from itself is always 0
		_distanceArray[_startingNode] = 0;
		int[][] times = graph.getTimes();

		// Find shortest path for every node
		for (int currentNode = 0; currentNode < nodesCount - 1; currentNode++) {
			int closerNeighbor = minDistance(_distanceArray, _visited);

			// Mark the picked node as processed
			_visited[closerNeighbor] = true;
			for (int destinationNode = 0; destinationNode < nodesCount; destinationNode++) {
				if (!_visited[destinationNode] && // Shortest path false
				times[closerNeighbor][destinationNode] != 0 && // Path neighbor-destinationNode exists
				_distanceArray[closerNeighbor] != Integer.MAX_VALUE && // distance found
						_distanceArray[closerNeighbor]
								+ times[closerNeighbor][destinationNode] < _distanceArray[destinationNode]) 
				{ 
					_distanceArray[destinationNode] = _distanceArray[closerNeighbor]
							+ times[closerNeighbor][destinationNode];
					_parents[currentNode] = closerNeighbor;

				}
			}
		}
		printSolution(startingNode, _distanceArray, _parents);
		//
		// for(int i = 0; i < _distanceArray.length; i++) {
		// System.out.println(i +" has "+ _distanceArray[i]);
		// }
		// checkCoal();
		// if (_candidates.size() > 0) {
		// buildFinalPath();
		// }else {
		// throw new RuntimeException("No existe un camino posible");
		// }
	}

	private static void printSolution(int startVertex, int[] distances, int[] parents) {
		int nVertices = distances.length;
		System.out.print("Nodo\t Distancia\t Ruta");

		for (int nodeIndex = 0; nodeIndex < nVertices; nodeIndex++) {
			if (nodeIndex != startVertex) {
				System.out.print("\n" + startVertex + " -> ");
				System.out.print(nodeIndex + " \t\t ");
				System.out.print(distances[nodeIndex] + "\t\t");
				printPath(nodeIndex, parents);
			}
		}
		System.out.println("");
	}

	private static void printPath(int currentVertex, int[] parents) {

		// Base case : Source node has
		// been processed
		if (currentVertex == -1) {
			return;
		}
		if (parents.length==0)
			System.out.print("no camino");
		else {
			printPath(parents[currentVertex], parents);
			System.out.print(currentVertex + " ");
		}
	}

	private static int minDistance(int distanceArray[], Boolean boolShortestPathSet[]) {
		// Initialize min value
		int min = Integer.MAX_VALUE;
		int min_index = -1;

		for (int node = 0; node < distanceArray.length; node++) {
			if (boolShortestPathSet[node] == false && distanceArray[node] <= min) {
				min = distanceArray[node];
				min_index = node;
			}
		}
		return min_index;
	}

	@SuppressWarnings("unused")
	private static void checkCoal() {
		for (int node = 0; node < _distanceArray.length; node++) {
			if (_distanceArray[node] != Integer.MAX_VALUE && !_graph.getCoal(node)) {
				_distanceArray[node] = Integer.MAX_VALUE;
			} else {
				_candidates.add(node);
			}
		}
	}

	@SuppressWarnings("unused")
	private static void buildFinalPath() {
		System.out.println("Candidates are: " + _candidates);
		System.out.println("Path has: " + _parents);
		// if(_candidates.size() == 0) { // Only one candidate
		// buildList(_candidates.get(0));
		// }
		// int candidateNode = _candidates.get(0);
		// int candidateDistance = _distanceArray[_candidates.get(0)];
		//
		//
		// for(int node : _candidates) {
		// if(_distanceArray[node]< candidateDistance) {
		// candidateDistance = _distanceArray[node];
		// candidateNode = node;
		// System.out.println("Candidate "+ node);
		// }
		// }
	}

	@SuppressWarnings("unused")
	private static void buildList(int destinationNode) {
		// ArrayList <Integer> finalList = new ArrayList <Integer> ();
		// while(destinationNode != _startingNode) {
		// finalList.add(_path.get(destinationNode));
		// }

	}

}
